import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'kelvinToCelsius' })
export class ExponentialStrengthPipe implements PipeTransform {
	transform(value: number): number {
		return Number((value - 273.15).toFixed(0));
	}
}
