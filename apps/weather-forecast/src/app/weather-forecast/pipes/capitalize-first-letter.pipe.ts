import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'capitalizeFirstLetter' })
export class CapitalizeFirstLetter implements PipeTransform {
	transform(value?: string): string {
		return (value && value[0].toUpperCase() + value.slice(1)) || '';
	}
}
