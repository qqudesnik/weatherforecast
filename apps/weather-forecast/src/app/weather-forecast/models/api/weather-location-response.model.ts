import { WeatherDayItemResponse } from './weather-day-item-response.model';
import { WeatherTimeItemResponse } from './weather-hour-item-response.model';

export interface WeatherLocationResponse {
	lat: number;
	lon: number;
	hourly: Array<WeatherTimeItemResponse>;
	daily: Array<WeatherDayItemResponse>;
}
