export interface WeatherTimeItemResponse {
	dt: number;
	temp: number;
}
