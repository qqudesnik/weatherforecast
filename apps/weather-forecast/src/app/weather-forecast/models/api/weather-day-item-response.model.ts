export interface WeatherDayItemResponse {
	dt: number;
	temp: { day: number };
}
