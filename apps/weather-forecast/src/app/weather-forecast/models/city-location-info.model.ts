export interface CityLocationInfo {
	name: string;
	lat: number;
	lon: number;
}
