import { WeatherTimeItemResponse } from './api/weather-hour-item-response.model';

export interface CityTemperatureInfo {
	daily?: Array<WeatherTimeItemResponse>;
	hourly?: Array<WeatherTimeItemResponse>;
}
