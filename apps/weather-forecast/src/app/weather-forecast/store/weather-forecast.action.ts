import { createAction, props } from '@ngrx/store';
import { WeatherTimeItemResponse } from '../models/api/weather-hour-item-response.model';
import { WeatherSearchMode } from '../models/api/weather-search-mode';
import { CityLocationInfo } from '../models/city-location-info.model';

export const loadCityLocationInfo = createAction('[Weather Forecast] loadCityLocationInfo', props<{ name: string }>());
export const cityLocationInfoLoaded = createAction(
	'[Weather Forecast] cityLocationInfoLoaded',
	props<{ cityLocationInfo: CityLocationInfo }>()
);
export const errorOccured = createAction('[Weather Forecast] errorOccured', props<{ message: string }>());

export const loadCityTemperature = createAction(
	'[Weather Forecast] loadCityTemperature',
	props<{ cityName: string; mode: WeatherSearchMode }>()
);
export const cityDailyTemperatureLoaded = createAction(
	'[Weather Forecast] cityDailyTemperatureLoaded',
	props<{ cityName: string; daily: Array<WeatherTimeItemResponse> }>()
);
export const cityHourlyTemperatureLoaded = createAction(
	'[Weather Forecast] cityHourltTemperatureLoaded',
	props<{ cityName: string; hourly: Array<WeatherTimeItemResponse> }>()
);
