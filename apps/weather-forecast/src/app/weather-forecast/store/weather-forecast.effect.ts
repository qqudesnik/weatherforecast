import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { exhaustMap, map, mergeMap, of, tap, withLatestFrom } from 'rxjs';
import { WeatherTimeItemResponse } from '../models/api/weather-hour-item-response.model';
import { WeatherLocationResponse } from '../models/api/weather-location-response.model';
import { WeatherSearchMode } from '../models/api/weather-search-mode';
import { CityLocationInfo } from '../models/city-location-info.model';
import { CityTemperatureInfo } from '../models/city-temperature-info.model';
import { WeatherApiService } from '../services/weather-api.service';
import * as WeatherForecastActions from './weather-forecast.action';
import { selectCityInfo, selectCityTemperatures } from './weather-forecast.selector';

@Injectable()
export class WeatherForecastEffects {
	loadCityInfo$ = createEffect(() =>
		this.actions$.pipe(
			ofType(WeatherForecastActions.loadCityLocationInfo),
			map(action => ({ ...action, name: action.name.toLocaleLowerCase() })),
			withLatestFrom(this.store.select(selectCityInfo)),
			mergeMap(([action, citiesInfo]) => {
				const cityInfo = citiesInfo && citiesInfo[action.name];

				if (cityInfo) {
					return of(
						WeatherForecastActions.cityLocationInfoLoaded({
							cityLocationInfo: { ...cityInfo, name: action.name },
						})
					);
				}

				return this.weatherApiService.getCityInfo(action.name).pipe(
					map(cityInfos => {
						if (cityInfos && cityInfos.length > 0) {
							return WeatherForecastActions.cityLocationInfoLoaded({ cityLocationInfo: cityInfos[0] });
						} else {
							return WeatherForecastActions.errorOccured({
								message: `City '${action.name}' was not found`,
							});
						}
					})
				);
			})
		)
	);

	loadDailyTemperature$ = createEffect(() =>
		this.actions$.pipe(
			ofType(WeatherForecastActions.loadCityTemperature),
			tap(action => this.store.dispatch(WeatherForecastActions.loadCityLocationInfo({ name: action.cityName }))),
			exhaustMap(() => this.actions$.pipe(ofType(WeatherForecastActions.cityLocationInfoLoaded))),
			withLatestFrom(this.actions$.pipe(ofType(WeatherForecastActions.loadCityTemperature))),
			withLatestFrom(this.store.select(selectCityTemperatures)),
			map(([[{ cityLocationInfo }, { mode }], cityTemperatures]) => ({
				cityLocationInfo,
				mode,
				cityTemperatures,
			})),
			mergeMap(({ cityLocationInfo, mode, cityTemperatures }) => {
				const cityName = cityLocationInfo.name.toLocaleLowerCase();
				const cached = cityTemperatures?.[cityName]?.[mode] || {};

				if (Array.isArray(cached) && cached.length > 0) {
					return this.getCityTemperatureFromCache(cityTemperatures, cityName, cityLocationInfo, mode);
				} else {
					return this.getWeatherFromApi(cityLocationInfo, mode, cityName);
				}
			}),
			map(result => {
				switch (result.mode) {
					case 'daily':
						return WeatherForecastActions.cityDailyTemperatureLoaded({
							cityName: result.cityName,
							daily: result.daily,
						});
					case 'hourly':
						return WeatherForecastActions.cityHourlyTemperatureLoaded({
							cityName: result.cityName,
							hourly: result.hourly,
						});
				}
			})
		)
	);

	constructor(private actions$: Actions, private weatherApiService: WeatherApiService, private store: Store) {}

	private getCityTemperatureFromCache(
		cityTemperatures: Record<string, CityTemperatureInfo> | null,
		cityName: string,
		cityLocationInfo: CityLocationInfo,
		mode: WeatherSearchMode
	) {
		return of(cityTemperatures?.[cityName]).pipe(
			map(cachedTemperatureData => {
				const result = {
					daily: cachedTemperatureData?.daily || [],
					hourly: cachedTemperatureData?.hourly || [],
					lat: cityLocationInfo.lat,
					lon: cityLocationInfo.lon,
					cityName: cityName,
					mode: mode,
				};

				return result;
			})
		);
	}

	private getWeatherFromApi(cityLocationInfo: CityLocationInfo, mode: WeatherSearchMode, cityName: string) {
		return this.weatherApiService.getWeather(cityLocationInfo.lat, cityLocationInfo.lon, mode).pipe(
			map(weatherResponse => {
				return this.convertDailyResponses(weatherResponse);
			}),
			map(weatherResponse => ({
				daily: ((<unknown>weatherResponse.daily) as Array<WeatherTimeItemResponse>) || [],
				hourly: weatherResponse.hourly || [],
				lat: cityLocationInfo.lat,
				lon: cityLocationInfo.lon,
				cityName: cityName,
				mode: mode,
			}))
		);
	}

	convertDailyResponses(weatherResponse: WeatherLocationResponse) {
		if (weatherResponse.daily != null) {
			((<unknown>weatherResponse.daily) as Array<WeatherTimeItemResponse>) = weatherResponse.daily.map(
				daily =>
					({
						dt: daily.dt,
						temp: daily.temp.day,
					} as WeatherTimeItemResponse)
			);
		}
		return weatherResponse;
	}
}
