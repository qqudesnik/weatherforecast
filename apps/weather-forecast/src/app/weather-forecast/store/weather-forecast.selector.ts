import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromWeatherForecast from './weather-forecast.reducer';

export const selectWeatherForecastState = createFeatureSelector<fromWeatherForecast.State>(
	fromWeatherForecast.weatherForecastFeature.name
);

export const selectCityInfo = createSelector(selectWeatherForecastState, state => state.cityLocationsInfo);
export const selectErrorMessage = createSelector(selectWeatherForecastState, state => state.errorMessage);

export const selectCityTemperatures = createSelector(selectWeatherForecastState, state => state.cityTemperatures);
