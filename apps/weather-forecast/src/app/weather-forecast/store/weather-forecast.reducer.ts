import { createFeature, createReducer, on } from '@ngrx/store';
import { CityLocationInfo } from '../models/city-location-info.model';
import { CityTemperatureInfo } from '../models/city-temperature-info.model';
import * as WeatherForecastActions from './weather-forecast.action';

export interface State {
	cityLocationsInfo: Record<string, CityLocationInfo>;
	cityTemperatures: Record<string, CityTemperatureInfo> | null;
	errorMessage: string | null;
}

const initialState: State = {
	cityLocationsInfo: {},
	cityTemperatures: null,
	errorMessage: null,
};

export const weatherForecastFeature = createFeature({
	name: 'weatherForecast',
	reducer: createReducer(
		initialState,

		on(WeatherForecastActions.cityLocationInfoLoaded, (state, payload) => {
			const newCityInfoMap = { ...state.cityLocationsInfo };
			newCityInfoMap[payload.cityLocationInfo.name.toLocaleLowerCase()] = payload.cityLocationInfo;

			return {
				...state,
				cityLocationsInfo: newCityInfoMap,
				errorMessage: null,
			};
		}),
		on(WeatherForecastActions.errorOccured, (state, payload) => {
			return {
				...state,
				errorMessage: payload.message,
			};
		}),
		on(WeatherForecastActions.cityDailyTemperatureLoaded, (state, payload) => {
			const newCityTemperatures = createCityTemperatures(payload.cityName, state);

			newCityTemperatures[payload.cityName].daily = payload.daily;

			return {
				...state,
				cityTemperatures: newCityTemperatures,
			};
		}),
		on(WeatherForecastActions.cityHourlyTemperatureLoaded, (state, payload) => {
			const newCityTemperatures = createCityTemperatures(payload.cityName, state);

			newCityTemperatures[payload.cityName].hourly = payload.hourly;

			return {
				...state,
				cityTemperatures: newCityTemperatures,
			};
		})
	),
});

function createCityTemperatures(cityName: string, state: State) {
	const cityTemperatures = { ...state.cityTemperatures };

	cityTemperatures[cityName] = {
		...cityTemperatures[cityName],
		hourly: [...((cityTemperatures[cityName] || {}).hourly || [])],
		daily: [...((cityTemperatures[cityName] || {}).daily || [])],
	};

	return cityTemperatures;
}
