import { Component, OnDestroy } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { ROUTES } from '../../../shared/constants';
import { WeatherSearchMode } from '../../models/api/weather-search-mode';
import { WeatherForecastService } from '../../services/weather-forecast.service';
import * as WeatherForecastActions from '../../store/weather-forecast.action';
import * as fromWeatherForecast from '../../store/weather-forecast.reducer';
import { selectErrorMessage } from '../../store/weather-forecast.selector';
import { WeatherTableRow } from '../weather-table/models/weather-table-row.model';

@Component({
	selector: 'bp-weather-forecast',
	templateUrl: './weather-forecast.component.html',
	styleUrls: ['./weather-forecast.component.scss'],
})
export class WeatherForecastComponent implements OnDestroy {
	form = new FormGroup({
		cityName: new FormControl(),
		mode: new FormControl(),
	});

	get mode() {
		return this.form.get('mode')?.value;
	}

	set mode(value: WeatherSearchMode) {
		this.form.get('mode')?.setValue(value);
	}

	get cityName() {
		return this.form.get('cityName')?.value;
	}

	set cityName(value: string) {
		this.form.get('cityName')?.setValue(value);
	}

	dayTemperatures: Array<WeatherTableRow> = [];
	hourTemperatures: Array<WeatherTableRow> = [];
	cities: Array<string> = [];

	errorMessage$ = this.store.pipe(select(selectErrorMessage));

	dayTemperatures$ = this.weatherForecast.getTemperatures$('daily').pipe(map(data => this.mapToDaily(data)));

	hourTemperatures$ = this.weatherForecast.getTemperatures$('hourly').pipe(map(data => this.mapToHourly(data)));

	private subscription = new Subscription();
	private searchSubject = new Subject<void>();

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private store: Store<fromWeatherForecast.State>,
		private weatherForecast: WeatherForecastService
	) {
		this.route.params.subscribe(params => {
			this.mode = params.mode || 'daily';
			this.cityName = params.cityName || '';
		});

		if (this.cityName != '') {
			this.search(true);
		}

		this.subscription.add(
			this.dayTemperatures$.subscribe(data => {
				this.dayTemperatures = [...this.dayTemperatures, data];
			})
		);

		this.subscription.add(
			this.hourTemperatures$.subscribe(data => {
				this.hourTemperatures = [...this.hourTemperatures, data];
			})
		);

		this.subscription.add(
			this.weatherForecast.cities$.subscribe(cities => {
				this.cities = cities;
			})
		);

		this.subscription.add(
			this.searchSubject.pipe(debounceTime(300)).subscribe(() => {
				new Set([this.cityName, ...this.cities]).forEach(city =>
					this.store.dispatch(WeatherForecastActions.loadCityTemperature({ cityName: city, mode: this.mode }))
				);

				this.updateUrl();
			})
		);
	}

	ngOnDestroy(): void {
		this.subscription.unsubscribe();
	}

	search(fromNavigation: boolean = false) {
		if (fromNavigation || this.cityName === '') {
			return;
		}

		this.searchSubject.next();
	}

	private updateUrl() {
		this.router.navigate([
			ROUTES.weatherByCityAndMode.replace(':cityName', this.cityName).replace(':mode', this.mode),
		]);
	}

	private mapToHourly(data: WeatherTableRow): any {
		return { ...data, values: data.values.filter((v, i) => i % 3 === 0).slice(0, 8) };
	}

	private mapToDaily(data: WeatherTableRow): any {
		return { ...data, values: data.values.slice(0, 7) };
	}
}
