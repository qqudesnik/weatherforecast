import { Component, Input } from '@angular/core';
import { WeatherSearchMode } from '../../models/api/weather-search-mode';
import { WeatherTableRow } from './models/weather-table-row.model';
import { WeatherTimeInfo } from './models/weather-time-info.model';

@Component({
	selector: 'bp-weather-table',
	templateUrl: './weather-table.component.html',
	styleUrls: ['./weather-table.component.scss'],
})
export class WeatherTableComponent {
	@Input() citiesData!: Array<WeatherTableRow | null>;
	@Input() format!: WeatherSearchMode;

	trackByCityName(index: number, row: WeatherTimeInfo) {
		return row.time;
	}
}
