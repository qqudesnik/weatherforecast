import { WeatherTimeInfo } from './weather-time-info.model';

export interface WeatherTableRow {
	cityName: string;
	values: Array<WeatherTimeInfo>;
}
