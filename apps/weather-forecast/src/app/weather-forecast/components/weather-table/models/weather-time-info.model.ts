export interface WeatherTimeInfo {
	time: Date;
	tempValue: number;
}
