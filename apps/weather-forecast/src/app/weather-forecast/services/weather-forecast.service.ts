import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { distinct, filter, from, map, mergeMap, Observable, withLatestFrom } from 'rxjs';
import { WeatherTableRow } from '../components/weather-table/models/weather-table-row.model';
import { WeatherTimeInfo } from '../components/weather-table/models/weather-time-info.model';
import { WeatherTimeItemResponse } from '../models/api/weather-hour-item-response.model';
import { WeatherSearchMode } from '../models/api/weather-search-mode';
import { CityTemperatureInfo } from '../models/city-temperature-info.model';
import { selectCityInfo, selectCityTemperatures } from '../store/weather-forecast.selector';

@Injectable({
	providedIn: 'root',
})
export class WeatherForecastService {
	cities$ = this.store.pipe(
		select(selectCityInfo),
		map(data => Object.keys(data as object))
	);

	temperatures$ = this.store.pipe(select(selectCityTemperatures)).pipe(
		filter(temperatures => temperatures != null),
		mergeMap(temperatures => from(Object.keys(temperatures as object))),
		withLatestFrom(this.store.select(selectCityTemperatures))
	);

	constructor(private store: Store) {}

	getTemperatures$(mode: WeatherSearchMode): Observable<any> {
		return this.temperatures$.pipe(
			filter(
				([city, temperatures]) =>
					(mode === 'hourly' &&
						(temperatures?.[city]?.hourly as Array<WeatherTimeItemResponse>).length > 0) ||
					(mode === 'daily' && (temperatures?.[city]?.daily as Array<WeatherTimeItemResponse>).length > 0)
			),
			this.mapToTableRow(mode),
			this.distinctTableRows()
		);
	}

	private distinctTableRows() {
		return function <T>(source: Observable<T>): Observable<T> {
			return source.pipe(distinct(data => JSON.stringify(data)));
		};
	}

	private mapToTableRow(mode: WeatherSearchMode) {
		return function (observable: Observable<any>) {
			return observable.pipe(
				map(([city, temperatures]) => {
					const weatherTimeInfos: Array<WeatherTimeInfo> =
						(
							(temperatures[city] as Record<string, CityTemperatureInfo>)[
								mode
							] as Array<WeatherTimeItemResponse>
						).map(t => ({
							tempValue: t.temp,
							time: new Date(t.dt * 1000),
						})) || [];

					const result: WeatherTableRow = {
						cityName: city,
						values: weatherTimeInfos,
					};

					return result;
				})
			);
		};
	}
}
