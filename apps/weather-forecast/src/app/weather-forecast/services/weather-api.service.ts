import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'apps/weather-forecast/src/environments/environment';
import { Observable } from 'rxjs';
import { WeatherLocationResponse } from '../models/api/weather-location-response.model';
import { WeatherSearchMode } from '../models/api/weather-search-mode';
import { CityLocationInfo } from '../models/city-location-info.model';

@Injectable({
	providedIn: 'root',
})
export class WeatherApiService {
	private readonly allSearchModes = ['current', 'minutely', 'hourly', 'daily', 'alert'];
	constructor(private http: HttpClient) {}

	getCityInfo(name: string): Observable<Array<CityLocationInfo>> {
		const url = 'http://api.openweathermap.org/geo/1.0/direct';

		const queryParams = new HttpParams()
			.append('q', name)
			.append('limit', 1)
			.append('appid', environment.weahterApiKey);

		return this.http.get<Array<CityLocationInfo>>(url, { params: queryParams });
	}

	getWeather(lat: number, lon: number, mode: WeatherSearchMode = 'hourly'): Observable<WeatherLocationResponse> {
		const url = `https://api.openweathermap.org/data/2.5/onecall`;
		const exclude = this.allSearchModes.filter(x => x != mode).join(',');

		const queryParams = new HttpParams()
			.append('lat', lat)
			.append('lon', lon)
			.append('exclude', exclude)
			.append('appid', environment.weahterApiKey);

		return this.http.get<WeatherLocationResponse>(url, { params: queryParams });
	}
}
