import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from '../shared/shared.module';
import { WeatherForecastComponent } from './components/weather-forecast/weather-forecast.component';
import { WeatherTableComponent } from './components/weather-table/weather-table.component';
import { CapitalizeFirstLetter } from './pipes/capitalize-first-letter.pipe';
import { ExponentialStrengthPipe } from './pipes/kelvin-to-celsius.pipe';
import { WeatherForecastEffects } from './store/weather-forecast.effect';
import { weatherForecastFeature } from './store/weather-forecast.reducer';

@NgModule({
	declarations: [WeatherForecastComponent, WeatherTableComponent, ExponentialStrengthPipe, CapitalizeFirstLetter],
	providers: [],
	imports: [
		CommonModule,
		SharedModule,
		FormsModule,
		ReactiveFormsModule,
		StoreModule.forFeature(weatherForecastFeature),
		EffectsModule.forFeature([WeatherForecastEffects]),
	],
	exports: [WeatherForecastComponent],
})
export class WeatherForecastModule {}
