import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WeatherForecastModule } from './weather-forecast/weather-forecast.module';

@NgModule({
	declarations: [AppComponent],
	imports: [BrowserModule, AppRoutingModule, WeatherForecastModule, StoreModule.forRoot({}), EffectsModule.forRoot()],
	providers: [],
	bootstrap: [AppComponent],
})
export class AppModule {}
