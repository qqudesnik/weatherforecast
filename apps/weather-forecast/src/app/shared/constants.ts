export enum ROUTES {
	init = '',
	weatherByCity = 'city/:cityName',
	weatherByCityAndMode = 'city/:cityName/:mode',
}
