import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ROUTES } from './shared/constants';
import { WeatherForecastComponent } from './weather-forecast/components/weather-forecast/weather-forecast.component';

const routesConfig: Routes = [
	{ path: ROUTES.init, component: WeatherForecastComponent },
	{ path: ROUTES.weatherByCity, component: WeatherForecastComponent },
	{ path: ROUTES.weatherByCityAndMode, component: WeatherForecastComponent },
];

@NgModule({
	imports: [RouterModule.forRoot(routesConfig)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
